package org.szhao.personio

import com.fasterxml.jackson.databind.ObjectMapper
import org.jooq.DSLContext
import org.jooq.generated.tables.references.RELATIONSHIP
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.szhao.personio.service.CacheService


@SpringBootTest
@AutoConfigureMockMvc
class PersonioApplicationTest @Autowired constructor(
    val mockMvc: MockMvc,
    val cacheService: CacheService,
    val dslContext: DSLContext
) {
    @BeforeEach
    fun init() {
        dslContext.deleteFrom(RELATIONSHIP).execute()
        cacheService.clearCache()
    }

    @AfterEach
    fun clear() {
        init()
    }

    @Test
    fun `create a hierarchy should return the hierarchy if it's successful`() {
        val json = """
            { "Pete": "Nick", "Barbara": "Nick", "Nick": "Sophie", "Sophie": "Jonas" }
        """.trimIndent()
        val response = mockMvc.perform(
            MockMvcRequestBuilders
                .post("/relations")
                .content(json)
                .header("Authorization", "secret-key-here!")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isAccepted)
            .andReturn()

        val result = ObjectMapper().readTree(response.response.contentAsString)
        val expected = ObjectMapper().readTree(
            """
            { 
              "Jonas": { 
                "Sophie": { 
                  "Nick": { 
                    "Pete": {}, 
                    "Barbara": {}
                  }
                }
              }
            }
        """.trimIndent()
        )

        assertEquals(expected, result)
    }

    @Test
    fun `cyclic relation will be rejected`() {
        val json = """
            { "Pete": "Nick", "Barbara": "Nick", "Nick": "Sophie", "Sophie": "Barbara" }
        """.trimIndent()
        val response = mockMvc.perform(
            MockMvcRequestBuilders
                .post("/relations")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "secret-key-here!")
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isBadRequest)
            .andReturn()
        val resp = ObjectMapper().readTree(response.response.contentAsString)
        assertNotNull(resp["message"])
    }

    @Test
    fun `get hierarchy should return a list of strings`() {
        val json = """
            { "Pete": "Nick", "Barbara": "Nick", "Nick": "Sophie", "Sophie": "Jonas" }
        """.trimIndent()
        mockMvc.perform(
            MockMvcRequestBuilders
                .post("/relations")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "secret-key-here!")
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isAccepted)
            .andReturn()

        val supervisors = mockMvc.perform(
            MockMvcRequestBuilders
                .get("/supervisors?user=Barbara")
                .header("Authorization", "secret-key-here!")
        )
            .andExpect(status().isOk)
            .andReturn()
        val obj = ObjectMapper().readValue(supervisors.response.contentAsString, List::class.java)
        assertEquals(listOf("Nick", "Sophie"), obj)
    }
}
