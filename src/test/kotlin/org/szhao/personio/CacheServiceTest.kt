package org.szhao.personio

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.szhao.personio.repo.CyclicRelationException
import org.szhao.personio.service.CacheService
import java.util.stream.Stream


class CacheServiceTest {
    private val cacheService = CacheService()

    @BeforeEach
    fun init() {
        cacheService.clearCache()
    }

    @ParameterizedTest
    @MethodSource("singleRootTwoLevels")
    fun `create single root two levels hierarchy should succeed`(input: List<Pair<String, String>>) {
        input.forEach { (e, s) -> cacheService.upsertRelation(e, s) }
        val roots = cacheService.getRoots()
        assertEquals(1, roots.size)
        val root = roots[0]
        assertEquals(input[0].second, root.name)
        assertEquals(input.size, root.underlings.size)
        assertEquals(input.map { it.first }.toSet(), root.underlings.map { it.name }.toSet())
        assertNull(root.supervisor)
    }

    @Test
    fun `create two roots two levels hierarchy should succeed`() {
        cacheService.upsertRelation("B", "A")
        cacheService.upsertRelation("B", "C")

        val roots = cacheService.getRoots()
        assertEquals(2, roots.size)
        assertEquals(setOf("A", "C"), roots.map { it.name }.toSet())
        val nodes = cacheService.getEmployees()
        assertEquals("C", nodes["B"]?.supervisor?.name)

        val root = nodes["A"]
        assertNull(root?.supervisor)
        assertEquals(0, root?.underlings?.size)
    }

    @Test
    fun `get supervisors should return correct result`() {
        listOf("B" to "A", "C" to "A", "D" to "A", "E" to "A").forEach { (e, s) ->
            cacheService.upsertRelation(e, s)
        }
        assertEquals(listOf("A"), cacheService.getSupervisors("B", 2))
        assertEquals(emptyList<String>(), cacheService.getSupervisors("A", 2))
    }

    @Test
    fun `get supervisors should return correct result 2`() {
        listOf("B" to "A", "D" to "C", "C" to "B", "E" to "D").forEach { (e, s) ->
            cacheService.upsertRelation(e, s)
        }
        assertEquals(listOf("C", "B"), cacheService.getSupervisors("D",2))
        assertEquals(listOf("C", "B", "A"), cacheService.getSupervisors("D", 3))
    }

    @ParameterizedTest
    @MethodSource("cyclicRelations")
    fun `should throw exception where there's cycle`(input: List<Pair<String, String>>) {
        assertThrows<CyclicRelationException> {
            input.forEach { (e, s) ->
                cacheService.upsertRelation(e, s)
            }
        }
    }

    companion object {
        @JvmStatic
        fun singleRootTwoLevels(): Stream<Arguments> = Stream.of(
            Arguments.of(listOf("B" to "A")),
            Arguments.of(listOf("B" to "A", "C" to "A", "D" to "A", "E" to "A")),
        )

        @JvmStatic
        fun cyclicRelations(): Stream<Arguments> = Stream.of(
            Arguments.of(listOf("A" to "A")),
            Arguments.of(listOf("B" to "A", "A" to "B")),
            Arguments.of(listOf("B" to "A", "C" to "B", "A" to "C")),
        )
    }

}