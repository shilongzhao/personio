package org.szhao.personio

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.szhao.personio.repo.EmployeeSerializer
import org.szhao.personio.service.Employee

@SpringBootTest
class EmployeeSerializerTest @Autowired constructor(
    val serializer: EmployeeSerializer
) {


    @Test
    fun `serialize an employ should succeed`() {
        val employee = Employee("A", mutableSetOf())
        val gen = serializer.serialize(employee)
        assertTrue(gen.has("A"))
        assertTrue(gen.get("A").isEmpty)

        val read = ObjectMapper().readTree("""{"A":{}}""")
        assertEquals(read, gen)
    }

    @Test
    fun `serialize multi-level hierarchy should succeed`() {
        val a = Employee("A", mutableSetOf())
        val b = Employee("B", a, mutableSetOf())
        val c = Employee("C", a, mutableSetOf())
        a.underlings.add(b)
        a.underlings.add(c)

        val gen = serializer.serialize(a)
        val read = ObjectMapper().readTree(
            """
            {
              "A": {
                "B": {},
                "C": {}
              }
            }
        """.trimIndent()
        )
        assertEquals(read, gen)
    }

    @Test
    fun `serialize multi-root hierarchy should succeed`() {
        val a = Employee("A", mutableSetOf())
        val b = Employee("B", a, mutableSetOf())
        val c = Employee("C", a, mutableSetOf())
        a.underlings.add(b)
        a.underlings.add(c)

        val x = Employee("X", mutableSetOf())
        val y = Employee("Y", x, mutableSetOf())
        val z = Employee("Z", x, mutableSetOf())
        x.underlings.add(y)
        x.underlings.add(z)

        val gen = serializer.serialize(setOf(a, x))
        val read = ObjectMapper().readTree(
            """
            {
              "A": {
                "B": {},
                "C": {}
              },
              "X": {
                "Y": {},
                "Z": {}
              }
            }
        """.trimIndent()
        )
        assertEquals(read, gen)
    }

    @Test
    fun `serialize general hierarchy should succeed`() {
        val a = Employee("A", mutableSetOf())
        val b = Employee("B", a, mutableSetOf())
        val c = Employee("C", a, mutableSetOf())
        a.underlings.add(b)
        a.underlings.add(c)

        val d = Employee("D", b, mutableSetOf())
        val e = Employee("E", b, mutableSetOf())
        b.underlings.add(d)
        b.underlings.add(e)

        val gen = serializer.serialize(a)
        val read = ObjectMapper().readTree(
            """
            {
              "A": {
                "B": {
                  "D": {},
                  "E": {}
                },
                "C": {}
              }
            } 
        """.trimIndent()
        )

        assertEquals(read, gen)
    }
}
