package org.szhao.personio

import org.jooq.DSLContext
import org.jooq.generated.tables.references.RELATIONSHIP
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.szhao.personio.repo.Relationship
import org.szhao.personio.repo.RelationshipRepo

@SpringBootTest
class RelationshipRepoTest @Autowired constructor(
    val dslContext: DSLContext,
    val relationshipRepo: RelationshipRepo
) {
    @BeforeEach
    fun init() {
        dslContext.deleteFrom(RELATIONSHIP).execute()
    }

    @AfterEach
    fun clear() {
        dslContext.deleteFrom(RELATIONSHIP).execute()
    }

    @Test
    fun `insert hierarchy with multi-levels should succeed`() {
        val input = listOf("D" to "C", "B" to "A", "C" to "B")
        input.forEach { (e, s) ->
            val r = relationshipRepo.upsertRelation(e, s)
            assertEquals(Relationship(e, s), r)
        }
        assertEquals(listOf("B"), relationshipRepo.getEmployees("A"))
        assertEquals("A", relationshipRepo.getSupervisor("B"))
        assertEquals("B", relationshipRepo.getSupervisor("C"))
        assertEquals("C", relationshipRepo.getSupervisor("D"))
        assertEquals(listOf("B"), relationshipRepo.getEmployees("A"))
        assertEquals(listOf("C"), relationshipRepo.getEmployees("B"))
        assertEquals(listOf("D"), relationshipRepo.getEmployees("C"))
    }

    @Test
    fun `insert hierarchy with flat structure should succeed`() {
        val input = listOf("D" to "A", "B" to "A", "C" to "A")
        input.forEach { (e, s) ->
            val r = relationshipRepo.upsertRelation(e, s)
            assertEquals(Relationship(e, s), r)
        }
        assertEquals(listOf("B", "C", "D").toSet(), relationshipRepo.getEmployees("A").toSet())
        assertEquals("A", relationshipRepo.getSupervisor("B"))
        assertEquals("A", relationshipRepo.getSupervisor("C"))
        assertEquals("A", relationshipRepo.getSupervisor("D"))
    }

    @Test
    fun `update hierarchy should succeed`() {
        val input = listOf("B" to "A", "C" to "A", "C" to "B")
        input.forEach { (e, s) ->
            val r = relationshipRepo.upsertRelation(e, s)
            assertEquals(Relationship(e, s), r)
        }
        assertEquals(listOf("B"), relationshipRepo.getEmployees("A"))
        assertEquals("A", relationshipRepo.getSupervisor("B"))
        assertEquals("B", relationshipRepo.getSupervisor("C"))
    }
}