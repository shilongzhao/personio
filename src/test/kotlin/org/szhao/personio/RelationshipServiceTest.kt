package org.szhao.personio

import org.jooq.DSLContext
import org.jooq.generated.tables.references.RELATIONSHIP
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.szhao.personio.repo.CyclicRelationException
import org.szhao.personio.repo.Relationship
import org.szhao.personio.service.CacheService
import org.szhao.personio.service.RelationshipService

@SpringBootTest
class RelationshipServiceTest @Autowired constructor(
    val dslContext: DSLContext,
    val cacheService: CacheService,
    val relationshipService: RelationshipService
) {
    @BeforeEach
    fun init() {
        dslContext.deleteFrom(RELATIONSHIP).execute()
        cacheService.clearCache()
    }

    @AfterEach
    fun clear() {
        init()
    }

    @Test
    fun `cyclic relationship should through exception`() {
        val pairs = listOf("A" to "B", "B" to "C", "B" to "D", "D" to "A")
        pairs.take(3).forEach { (e, s) ->
            val r = relationshipService.createRelationship(e, s)
            assertEquals(Relationship(e, s), r)
        }
        assertThrows<CyclicRelationException> {
           relationshipService.createRelationship(pairs.last().first, pairs.last().second)
        }
    }

    @Test
    fun `non cyclic relationship should succeed`() {
        val pairs = listOf("A" to "B", "B" to "C", "B" to "D", "C" to "D")
        pairs.forEach { (e, s) ->
            val r = relationshipService.createRelationship(e, s)
            assertEquals(Relationship(e, s), r)
        }

        val roots = relationshipService.getHierarchies()
        assertEquals(1, roots.size)
        assertEquals("D", roots[0].name)
    }
}