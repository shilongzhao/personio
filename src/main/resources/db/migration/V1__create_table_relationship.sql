
CREATE TABLE IF NOT EXISTS relationship(
    employee VARCHAR(256) NOT NULL UNIQUE,
    supervisor VARCHAR(256) NOT NULL
);

CREATE INDEX relation_supervisor_idx ON relationship(supervisor)
