package org.szhao.personio.service

import org.springframework.stereotype.Service
import org.szhao.personio.repo.CyclicRelationException

class Employee(val name: String, var supervisor: Employee?, val underlings: MutableSet<Employee>) {
    constructor(name: String, underlings: MutableSet<Employee>) : this(name, null, underlings)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Employee
        if (name != other.name) return false
        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

}

@Service
class CacheService {
    private val roots = mutableSetOf<Employee>()
    private val employees = mutableMapOf<String, Employee>()

    /**
     * e -> s: edge to add
     */
    fun hasCycle(employee: String, supervisor: String): Boolean {
        if (employee == supervisor) return true
        if (!employees.contains(employee) || !employees.contains(supervisor)) return false

        val under = employees[employee]
        var sup = employees[supervisor]
        while (sup?.supervisor != null) {
            sup = sup.supervisor
            if (sup?.name == under?.name) return true
        }
        return false
    }

    fun upsertRelation(employee: String, supervisor: String) {
        if (hasCycle(employee, supervisor)) throw CyclicRelationException("cycle $employee->$supervisor")

        val sNode = employees.getOrPut(supervisor) { Employee(supervisor, mutableSetOf()) }
        val eNode = employees.getOrPut(employee) { Employee(employee, mutableSetOf()) }

        eNode.supervisor?.underlings?.remove(eNode)

        eNode.supervisor = sNode
        sNode.underlings.add(eNode)

        roots.remove(eNode)
        if (sNode.supervisor == null) {
            roots.add(sNode)
        }
    }

    fun getSupervisors(employee: String, levelUp: Int): List<String> {
        if (levelUp == 0) return emptyList()

        val node = employees[employee] ?: return emptyList()
        val supervisor = node.supervisor ?: return emptyList()

        return listOf(supervisor.name) + getSupervisors(supervisor.name, levelUp - 1)

    }


    fun getRoots(): List<Employee> = roots.toList()
    fun getEmployees(): Map<String, Employee> = employees

    fun clearCache() {
        roots.clear()
        employees.clear()
    }
}