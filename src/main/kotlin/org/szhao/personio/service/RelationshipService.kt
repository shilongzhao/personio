package org.szhao.personio.service

import org.springframework.stereotype.Service
import org.szhao.personio.repo.CyclicRelationException
import org.szhao.personio.repo.Relationship
import org.szhao.personio.repo.RelationshipRepo

@Service
class RelationshipService(val relationshipRepo: RelationshipRepo, val cacheService: CacheService) {

    fun createRelationship(employee: String, supervisor: String): Relationship {
        if (cacheService.hasCycle(employee, supervisor))
            throw CyclicRelationException("relation $employee -> $supervisor causes cycle")
        val relationship = relationshipRepo.upsertRelation(employee, supervisor)
        cacheService.upsertRelation(employee, supervisor)
        return relationship
    }

    fun getHierarchies(): List<Employee> {
       return cacheService.getRoots()
    }

    fun getSupervisors(employee: String, levelUp: Int): List<String> {
        return cacheService.getSupervisors(employee, levelUp)
    }

}