package org.szhao.personio.repo

class CyclicRelationException(override val message: String) : RuntimeException(message)