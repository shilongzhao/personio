package org.szhao.personio.repo

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.springframework.stereotype.Component
import org.szhao.personio.service.Employee

@Component
class EmployeeSerializer {
    private val mapper = ObjectMapper()

    fun serialize(employees: Collection<Employee>): JsonNode {
        val root = mapper.createObjectNode()
        for (e in employees) {
            serialize(e, root)
        }
        return root
    }

    fun serialize(employee: Employee): ObjectNode {
        val root = mapper.createObjectNode()
        serialize(employee, root)
        return root
    }

    fun serialize(employee: Employee, parent: ObjectNode): ObjectNode {

        val next = mapper.createObjectNode()
        parent.set<JsonNode>(employee.name, next)

        for (e in employee.underlings) {
            serialize(e, next)
        }

        return parent
    }
}