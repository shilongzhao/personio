package org.szhao.personio.repo

import org.jooq.DSLContext
import org.jooq.generated.keys.RELATIONSHIP_EMPLOYEE_KEY
import org.jooq.generated.tables.references.RELATIONSHIP
import org.springframework.stereotype.Repository

data class Relationship(val employee: String, val supervisor: String)

@Repository
class RelationshipRepo(val dslContext: DSLContext) {

    fun upsertRelation(employee: String, supervisor: String): Relationship {
        return dslContext.insertInto(RELATIONSHIP, RELATIONSHIP.EMPLOYEE, RELATIONSHIP.SUPERVISOR)
            .values(employee, supervisor)
            .onConflict(RELATIONSHIP_EMPLOYEE_KEY.fields)
            .doUpdate()
            .set(RELATIONSHIP.SUPERVISOR, supervisor)
            .returning()
            .fetch().into(Relationship::class.java).first()
    }
    fun getSupervisor(employee: String): String {
        return dslContext.select(RELATIONSHIP.SUPERVISOR)
            .from(RELATIONSHIP)
            .where(RELATIONSHIP.EMPLOYEE.eq(employee))
            .fetch().into(String::class.java).first()
    }

    fun getEmployees(supervisor: String): List<String> {
        return dslContext.select(RELATIONSHIP.EMPLOYEE)
            .from(RELATIONSHIP)
            .where(RELATIONSHIP.SUPERVISOR.eq(supervisor))
            .fetch().into(String::class.java)
    }
}