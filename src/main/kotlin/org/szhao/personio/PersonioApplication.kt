package org.szhao.personio

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PersonioApplication

fun main(args: Array<String>) {
	runApplication<PersonioApplication>(*args)
}
