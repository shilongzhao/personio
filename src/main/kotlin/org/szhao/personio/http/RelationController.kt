package org.szhao.personio

import com.fasterxml.jackson.databind.JsonNode
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import org.szhao.personio.repo.CyclicRelationException
import org.szhao.personio.repo.EmployeeSerializer
import org.szhao.personio.service.RelationshipService

@RestController
class RelationController(val relationshipService: RelationshipService, val serializer: EmployeeSerializer) {

    @PostMapping("/relations")
    fun createRelations(@RequestBody map: Map<String, String>): ResponseEntity<JsonNode> {
        map.forEach { (employee, supervisor) ->
            relationshipService.createRelationship(employee, supervisor)
        }

        val roots = relationshipService.getHierarchies()
        return ResponseEntity.status(HttpStatus.ACCEPTED.value())
            .body(serializer.serialize(roots))
    }

    @GetMapping("/supervisors")
    fun getSupervisors(@RequestParam user: String): ResponseEntity<List<String>> {
        val list = relationshipService.getSupervisors(user, 2)
        return ResponseEntity.status(HttpStatus.OK.value())
            .body(list)
    }
}

@RestControllerAdvice
class RelationControllerAdvice: ResponseEntityExceptionHandler() {
    @ExceptionHandler(
        value = [CyclicRelationException::class]
    )
    fun badRequest(e: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        val errorResponse = ErrorResponse(HttpStatus.BAD_REQUEST.value(), e.message ?: "Error not specified")
        return handleExceptionInternal(e, errorResponse, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
    }
}

data class ErrorResponse(val status: Int, val message: String)