package org.szhao.personio.http

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AuthorizationInterceptor: HandlerInterceptor {
    private val secretKey = "secret-key-here!"
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val authorization = request.getHeader("Authorization")
        if (authorization == null || authorization != secretKey) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value())
            return false
        }
        return true
    }
}