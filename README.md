
Assumptions: 
- An employee only has one supervisor
- Error detection is and should be at insertion time
  - prevent errors instead of correcting errors
- There could be multiple roots 
- We can update the supervisors of employees

----
Instructions: 
1. run `docker compose up -d db` to start DB
2. run `./gradlew flywayMigrate` to migrate DB
3. run `./gradlew generateJooq` to generate jooq code
4. run `./gradlew test` to run all tests
5. run `./gradlew bootRun` to start the app
6. The secret to access the endpoints is "secret-key-here!", use it in the Authorization header